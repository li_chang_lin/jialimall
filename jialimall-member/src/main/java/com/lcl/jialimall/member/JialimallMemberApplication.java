package com.lcl.jialimall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.lcl.jialimall.member.feign")
@EnableDiscoveryClient
@SpringBootApplication
public class JialimallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run (JialimallMemberApplication.class, args);
    }

}
