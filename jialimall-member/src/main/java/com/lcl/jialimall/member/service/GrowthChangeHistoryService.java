package com.lcl.jialimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lcl.common.utils.PageUtils;
import com.lcl.jialimall.member.entity.GrowthChangeHistoryEntity;

import java.util.Map;

/**
 * 成长值变化历史记录
 *
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 15:26:38
 */
public interface GrowthChangeHistoryService extends IService<GrowthChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

