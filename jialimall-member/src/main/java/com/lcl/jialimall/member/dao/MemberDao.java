package com.lcl.jialimall.member.dao;

import com.lcl.jialimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 15:26:38
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
