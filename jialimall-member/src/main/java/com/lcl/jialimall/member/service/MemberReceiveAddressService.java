package com.lcl.jialimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lcl.common.utils.PageUtils;
import com.lcl.jialimall.member.entity.MemberReceiveAddressEntity;

import java.util.Map;

/**
 * 会员收货地址
 *
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 15:26:38
 */
public interface MemberReceiveAddressService extends IService<MemberReceiveAddressEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

