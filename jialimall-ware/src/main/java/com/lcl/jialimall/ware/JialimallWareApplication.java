package com.lcl.jialimall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JialimallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run (JialimallWareApplication.class, args);
    }

}
