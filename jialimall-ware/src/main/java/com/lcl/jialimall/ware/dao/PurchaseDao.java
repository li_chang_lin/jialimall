package com.lcl.jialimall.ware.dao;

import com.lcl.jialimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 15:42:58
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
