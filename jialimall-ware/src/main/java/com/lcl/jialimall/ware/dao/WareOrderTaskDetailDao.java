package com.lcl.jialimall.ware.dao;

import com.lcl.jialimall.ware.entity.WareOrderTaskDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 15:42:57
 */
@Mapper
public interface WareOrderTaskDetailDao extends BaseMapper<WareOrderTaskDetailEntity> {
	
}
