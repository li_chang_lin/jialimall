package com.lcl.jialimall.order.dao;

import com.lcl.jialimall.order.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 15:37:20
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {
	
}
