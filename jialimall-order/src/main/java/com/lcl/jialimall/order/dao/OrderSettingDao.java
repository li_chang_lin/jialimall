package com.lcl.jialimall.order.dao;

import com.lcl.jialimall.order.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 15:37:20
 */
@Mapper
public interface OrderSettingDao extends BaseMapper<OrderSettingEntity> {
	
}
