package com.lcl.jialimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lcl.common.utils.PageUtils;
import com.lcl.jialimall.order.entity.OrderReturnApplyEntity;

import java.util.Map;

/**
 * 订单退货申请
 *
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 15:37:20
 */
public interface OrderReturnApplyService extends IService<OrderReturnApplyEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

