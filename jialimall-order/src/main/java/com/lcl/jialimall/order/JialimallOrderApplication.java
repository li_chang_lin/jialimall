package com.lcl.jialimall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JialimallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run (JialimallOrderApplication.class, args);
    }

}
