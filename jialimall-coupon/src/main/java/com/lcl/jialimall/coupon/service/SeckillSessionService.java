package com.lcl.jialimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lcl.common.utils.PageUtils;
import com.lcl.jialimall.coupon.entity.SeckillSessionEntity;

import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 21:01:13
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

