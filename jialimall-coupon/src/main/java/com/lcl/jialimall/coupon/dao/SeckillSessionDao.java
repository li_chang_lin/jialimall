package com.lcl.jialimall.coupon.dao;

import com.lcl.jialimall.coupon.entity.SeckillSessionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动场次
 * 
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 21:01:13
 */
@Mapper
public interface SeckillSessionDao extends BaseMapper<SeckillSessionEntity> {
	
}
