package com.lcl.jialimall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class JialimallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run (JialimallCouponApplication.class, args);
    }

}
