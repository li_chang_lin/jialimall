package com.lcl.jialimall.coupon.dao;

import com.lcl.jialimall.coupon.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 * 
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 21:01:13
 */
@Mapper
public interface HomeAdvDao extends BaseMapper<HomeAdvEntity> {
	
}
