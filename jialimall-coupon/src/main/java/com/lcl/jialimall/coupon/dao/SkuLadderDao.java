package com.lcl.jialimall.coupon.dao;

import com.lcl.jialimall.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 * 
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 21:01:13
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {
	
}
