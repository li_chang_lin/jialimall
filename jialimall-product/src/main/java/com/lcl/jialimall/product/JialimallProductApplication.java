package com.lcl.jialimall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.lcl.jialimall.product.dao")
@SpringBootApplication
public class JialimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run (JialimallProductApplication.class, args);
    }

}
