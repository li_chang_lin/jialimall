package com.lcl.jialimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lcl.common.utils.PageUtils;
import com.lcl.jialimall.product.entity.SkuSaleAttrValueEntity;

import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 11:37:34
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

