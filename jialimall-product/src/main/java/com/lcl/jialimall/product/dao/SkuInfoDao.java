package com.lcl.jialimall.product.dao;

import com.lcl.jialimall.product.entity.SkuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku信息
 * 
 * @author lichanglin
 * @email 749755576@qq.com
 * @date 2020-04-24 11:37:34
 */
@Mapper
public interface SkuInfoDao extends BaseMapper<SkuInfoEntity> {
	
}
